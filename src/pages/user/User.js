import React, { Component } from 'react';
import UserForm from './UserForm';
import { Values } from "redux-form-website-template";
import saveForm from '../../actions/user/UserAction';

class User extends Component {

    render() {
        return (
            <div style={{ padding: 15 }}>
                <h2>User</h2>
                <UserForm onSubmit={saveForm} />
                <Values form="formUser" />
            </div>
        );
    }
}


export default (User)
